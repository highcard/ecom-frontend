// React
import React from "react";
import ReactDOM from "react-dom";

// Redux
import { Provider } from "react-redux";

// Router
import { ConnectedRouter } from "connected-react-router";

// Redux Setup
import { history, store } from "./redux";

// Components
import { App } from "./components/App";

// Service Worker
import * as serviceWorker from "./serviceWorker";

// Stylesheets
import "./index.scss";

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App/>
        </ConnectedRouter>
    </Provider>
    ,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
