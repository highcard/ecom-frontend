import { ProductsAction } from "../actions/products";
import { OrderAttribute, OrderDirection } from "../components/ProductList";
import { ProductId } from "../types/products";
import { IProductsState } from "../types/state";

// tslint:disable:object-literal-sort-keys
const initialProductsState: IProductsState = {
  visibleProducts: [
    {
      id: 1 as ProductId,
      name: "Highcard Shirt",
      description: "A high-quality shirt to show your affiliation with the coolest company on this planet.",
      price: 3500,
      isAvailable: false,
      images: [],
      createdAt: new Date("2019-02-11T01:55:00+0100"),
      updatedAt: new Date("2019-02-11T01:55:00+0100")
    }
  ],
  visibleOrder: OrderDirection.ASC,
  visibleOrderBy: OrderAttribute.NAME
};

export const productsReducer =
(state: IProductsState | undefined = initialProductsState, action: ProductsAction): IProductsState => {
  return state;
};
