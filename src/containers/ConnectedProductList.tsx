import { connect } from "react-redux";
import { Dispatch } from "redux";
import { ProductList } from "../components/ProductList";
import { IGlobalState, IProductsState } from "../types/state";

type TypeProps<B, T> = ({
  [P in keyof B]: B[P] extends T ? P : never
})[keyof B];

type StateKey<T> = TypeProps<IGlobalState, IProductsState>;

const mapStateWithMount = (mount: StateKey<IProductsState>) => {
  const mapStateToProps = (state: IGlobalState) => {
    const ps: IProductsState = state[mount];
    return {
      products: ps.visibleProducts,
      order: ps.visibleOrder,
      orderBy: ps.visibleOrderBy
    };
  };
  return mapStateToProps;
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    onProductLinkClick: () => {}
  };
};

export const ConnectedProductList = (mount: StateKey<IProductsState>) => (
  connect(mapStateWithMount(mount), mapDispatchToProps)(ProductList)
);
