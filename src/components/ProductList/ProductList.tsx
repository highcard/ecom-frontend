// React
import * as React from "react";

// Router
import { Link } from "react-router-dom";

// Entities
import { IProduct, ProductId } from "../../types/products";

export enum OrderAttribute {
  ID = "id",
  NAME = "name",
  CREATED_AT = "createdAt",
  UPDATED_AT = "updatedAt"
}

export enum OrderDirection {
  ASC = "asc",
  DESC = "desc"
}

export type ProductLinkClickCallback = (id: ProductId) => void;

interface IProps {
  // Ordering
  order: OrderDirection;
  orderBy: OrderAttribute;

  // Data
  products: IProduct[];

  // Callbacks
  onProductLinkClick: ProductLinkClickCallback;
}

export type IProductListProps = IProps;

export class ProductList extends React.Component<IProps> {
  public static defaultProps: Partial<IProps> = {
    order: OrderDirection.ASC,
    orderBy: OrderAttribute.NAME
  };

  public render() {
    const { products, onProductLinkClick } = this.props;
    return (
      <div className="product-list">
        {products.map(product => (
          <div className="item">
            <Link to={`/product/${product.id}`}>
              <h3>{product.name}</h3>
            </Link>
            <p>{product.description}</p>
          </div>
        ))}
      </div>
    );
  }
}
