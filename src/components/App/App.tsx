// React
import * as React from "react";
// Router
import { Route, Switch } from "react-router";
import { Link } from "react-router-dom";

// Stylesheets
import "./App.scss";

// Components
import { AllProductsPage, ConnectedErrorPage as ErrorPage, ErrorCode, HomePage, ProductPage } from "../../pages";

export class App extends React.Component {
  public render() {
    const E404 = () => <ErrorPage errorCode={ErrorCode.NotFound} />;
    return (
      <div id="App">
        <header>
          <Link to="/">Home</Link>
          <Link to="/all">All Products</Link>
        </header>
        <section className="content">
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route exact path="/all" component={AllProductsPage} />
            <Route exact path="/product/:id(\d+)" component={ProductPage} />
            <Route component={E404} />
          </Switch>
        </section>
      </div>
    );
  }
}
