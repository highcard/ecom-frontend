export const DUMMY: string = "products/DUMMY";
export type DummyAction = { type: typeof DUMMY; };
export const doDummy = (): DummyAction => {
  return {
    type: DUMMY
  };
};

export type ProductsAction = DummyAction;
