// Redux
import { applyMiddleware, combineReducers, createStore } from "redux";

// Router
import { connectRouter, routerMiddleware } from "connected-react-router";

// History
import createBrowserHistory from "history/createBrowserHistory";

// Redux Middlewares
import { middlewares } from "./middlewares";
// Redux Reducers
import { productsReducer } from "./reducers";

// Application History
const history = createBrowserHistory();

// Combine reducers to root reducer and include persistence
const reducer = combineReducers({
  products: productsReducer,
  router: connectRouter(history)
});

// Redux Store
const store = createStore(
  reducer,
  undefined,
  applyMiddleware(
    routerMiddleware(history),
    ...middlewares
  )
);

export { history, store };
