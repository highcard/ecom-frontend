import { Middleware } from "redux";
import { IGlobalState } from "./types/state";

const loggerMiddleware: Middleware<{}, IGlobalState> = (store) => (next) => (action) => {
  console.log(">>> dispatching", action);
  const result = next(action);
  console.log("<<< next state", store.getState());
  return result;
};

const middlewares: Middleware[] = [];

if (process.env.DEBUG || process.env.NODE_ENV !== "production") {
  middlewares.push(loggerMiddleware);
}
export { middlewares };
