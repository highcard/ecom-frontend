import * as React from "react";
import { RouteComponentProps } from "react-router";
import { ConnectedErrorPage as ErrorPage, ErrorCode } from "../ErrorPage";

interface IProps extends RouteComponentProps<{id?: string}> {}

export class ProductPage extends React.Component<IProps> {
  public render() {
    const { match } = this.props;

    if (match.params.id === undefined) {
      return <ErrorPage errorCode={ErrorCode.NotFound} />;
    }

    const id: number = parseInt(match.params.id, 10);
    return (
      <ErrorPage errorCode={ErrorCode.NotImplemented} />
    );
  }
}
