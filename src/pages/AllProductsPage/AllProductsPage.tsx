import * as React from "react";
import { OrderAttribute, OrderDirection } from "../../components/ProductList";
import { ConnectedProductList } from "../../containers/ConnectedProductList";

export class AllProductsPage extends React.Component {
  public render() {
    const ProductList = ConnectedProductList("products");
    return (
      <div>
        <p>
          We are the best!
        </p>
        <ProductList orderBy={OrderAttribute.NAME} order={OrderDirection.ASC} />
      </div>
    );
  }
}
