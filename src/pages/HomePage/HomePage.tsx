import * as React from "react";

import "./HomePage.scss";
import logo from "./logo.svg";

export class HomePage extends React.Component<{}> {
  public render() {
    return (
      <div id="HomePage">
        <img src={logo} className="logo" alt="logo" />
        <p>Hello World!</p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
        Learn React
        </a>
      </div>
    );
  }
}
