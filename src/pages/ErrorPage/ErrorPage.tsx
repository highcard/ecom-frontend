import { goBack } from "connected-react-router";
import * as React from "react";
import { connect } from "react-redux";

import "./ErrorPage.scss";

export enum ErrorCode {
  UnknownError = 0,
  BadRequest = 400,
  Unauthorized = 401,
  Forbidden = 403,
  NotFound = 404,
  MethodNotAllowed = 405,
  Gone = 410,
  UnavailableForLegalReasons = 451,
  InternalServerError = 500,
  NotImplemented = 501,
  ServiceUnavailable = 503
}

const messageMap: { [x: number]: string } = {
  [ErrorCode.UnknownError]: "An unknown error occured!",
  [ErrorCode.NotFound]: "The page you are looking for does not exist!",
  [ErrorCode.UnavailableForLegalReasons]: "If we showed this to you, we'd have to kill you!",
  [ErrorCode.InternalServerError]: "Oooof! Our server just exploded. D:",
  [ErrorCode.NotImplemented]: "Sorry! This feature is not implemented yet. ¯\\_(ツ)_/¯",
  [ErrorCode.ServiceUnavailable]: "Damn! We can't help you right now. Please come back later!"
};

interface IProps {
  errorCode: ErrorCode;
  errorMessage?: string;

  onBackLinkClick: () => void;
}

export class ErrorPage extends React.Component<IProps> {
  public static defaultProps: IProps = {
    errorCode: ErrorCode.UnknownError,
    errorMessage: undefined,
    onBackLinkClick: () => {}
  };

  public render() {
    const { errorCode, errorMessage } = this.props;
    const message = errorMessage === undefined ? messageMap[errorCode] : errorMessage;
    const handleBackLinkClick = (e: React.MouseEvent<HTMLAnchorElement>) => {
      e.preventDefault();
      this.props.onBackLinkClick();
    };
    return (
      <div className="error-page">
        <h1>Error {errorCode as number}</h1>
        <p className="message">{message}</p>
        <a className="back" href="#" onClick={handleBackLinkClick}>Bring me back!</a>
      </div>
    );
  }
}

export const ConnectedErrorPage = connect(null, { onBackLinkClick: goBack })(ErrorPage);
