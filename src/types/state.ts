import { LocationChangeAction, RouterState } from "connected-react-router";
import { ProductsAction } from "../actions/products";
import { OrderAttribute, OrderDirection } from "../components/ProductList";
import { IProduct } from "./products";

// Global
export interface IGlobalState {
  router: RouterState;
  products: IProductsState;
}

export type GlobalAction = LocationChangeAction | ProductsAction;

// Products
export interface IProductsState {
  visibleProducts: IProduct[];
  visibleOrder: OrderDirection;
  visibleOrderBy: OrderAttribute;
}
