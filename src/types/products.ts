import { Url } from "url";
import { OrderAttribute, OrderDirection } from "../components/ProductList";

type Opaque<T, K> = T & { __opaque__: K };

interface IEntity {
  createdAt: Date;
  updatedAt: Date;
}

export type ProductId = Opaque<number, "ProductId">;

export interface IProduct extends IEntity {
  id: ProductId;
  name: string;
  description: string;
  price: number;
  isAvailable: boolean;
  images: IProductImage[];
}

export type ProductImageId = Opaque<number, "ProductImageId">;

export interface IProductImage extends IEntity {
  id: ProductImageId;
  product?: IProduct;
  url: Url | string;
}
