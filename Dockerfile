## Stage 1: Build application
FROM node:lts-alpine as build-deps

WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
COPY . ./
RUN npm run build

## Stage 2: Deploy application
FROM nginx:stable-alpine

# Replace configuration
RUN rm -r /etc/nginx/*.conf
RUN rm -r /etc/nginx/conf.d/*
COPY ./nginx/ /etc/nginx

# Copy application
COPY --from=build-deps /usr/src/app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
